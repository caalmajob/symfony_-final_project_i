<?php


namespace App\Controller;


use App\Entity\Demo;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;


class DemoController extends AbstractController
{

    /**
     * @Route("/insert/demo")
     */
    public function insertDemo(EntityManagerInterface $doctrine, Request $request)
    {

        $nombre = $request->get('nombre');
        $email = $request->get('email');
        $ciudad = $request->get('ciudad');


        $user = new Demo();
        $user->setNombre("$nombre");
        $user->setCiudad("$email");
        $user->setEmail("$ciudad");

        $doctrine->persist($user);
        $doctrine->flush();

        return $this->redirectToRoute('maleteo_homepage');

    }
    

    /**
     * @Route("/solicitudes")
     */
    public function showListDemo(EntityManagerInterface $doctrine) {

        $repo = $doctrine->getRepository(Demo::class);

        $demo = $repo->findAll();

        return $this->render("/maleteo/show-demo.html.twig", ["demos"=>$demo]);
    }
}