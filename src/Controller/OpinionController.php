<?php


namespace App\Controller;


use App\Entity\Opinion;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;


class OpinionController extends AbstractController
{

    /**
     * @Route("/insert/opinion")
     */
    public function insertOpinion(EntityManagerInterface $doctrine, Request $request)
    {

        $comentario = $request->get('comentario');
        $autor = $request->get('autor');
        $ciudad = $request->get('ciudad');


        $user1 = new Opinion();
        $user1->setComentario("$comentario");
        $user1->setAutor("$autor");
        $user1->setCiudad("$ciudad");

        $doctrine->persist($user1);
        $doctrine->flush();

        // return $this->render("maleteo/demo.html.twig");
        return $this->redirectToRoute('maleteo_homepage');

    }

    /**
     * @Route("/showForm", name="formulario")
     */
    public function showFormNewOpinion()
    {
        return $this->render('maleteo/form-opinion.html.twig');
    }


}