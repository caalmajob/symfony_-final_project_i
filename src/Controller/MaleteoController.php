<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Opinion;


class MaleteoController extends AbstractController
{
/**
     * @Route("/")
     */
    public function redirectToLogin()
    {
        return $this->redirectToRoute('app_login');

    }



     /**
     * @Route ("/maleteo", name="maleteo_homepage")
     */
    public function showOpinion(EntityManagerInterface $doctrine)
    {
        $repo = $doctrine->getRepository(Opinion::class);

        $opinion = $repo->findby(array(),array('id'=>'DESC'),3,0);

        return $this->render("/maleteo/base.html.twig", ["opinions"=>$opinion]);

    }

}
